<?php
/**
Extra Theme settings
*/
function dstheme_form_system_theme_settings_alter(&$form, &$form_state) {
    $form['dstheme_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Datum Square Theme Settings'),
        '#collapsible' => False,
        '#collapsed' => False,
    );
    $form['dstheme_settings']['contact_info'] = array(
        '#type' => 'fieldset',
        '#title' => t('Contact Info'),
        '#collapsible' => True,
        '#collapsed' => True,
    );
    $form['dstheme_settings']['contact_info']['contact_email'] = array(
        '#type' => 'textfield',
        '#title' => t('Contact email'),
        '#default_value' => theme_get_setting('contact_email'),
        '#description'   => t("Enter the contact email."),
    );
    $form['dstheme_settings']['contact_info']['contact_phone'] = array(
        '#type' => 'textfield',
        '#title' => t('Contact Phone'),
        '#default_value' => theme_get_setting('contact_phone'),
        '#description'   => t("Enter the contact phone number."),
    );
}