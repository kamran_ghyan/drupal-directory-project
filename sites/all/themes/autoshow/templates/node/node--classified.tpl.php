<?php 
	/*echo "<pre>";
	print_r($content); 
	echo "</pre>";*/
?>
<?php hide($content); ?>
<div class="row-fluid detail-listing-view">
  <div class="col-md-8">
  	<div class="well">
    	<h1 class="detail-heading"><?php print render($content['classified_category']['#object']->title); ?></h1>
        <?php hide($content); ?>
        <p class="detail-sub-heading" style="font-size:12px;">
			<?php print render($content['field_city']); ?> 
            <span class="time"><?php print render($content['classified_category']); ?></span>
        </p>
        <div class="img-gallery mb40 pos-rel">
        	<div>
            	<?php print render($content['field_main_image']); ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <table class="table table-featured nomargin">
            <tbody>
            <tr>
              <td class="ad-data"><?php print t('Year'); ?></td>
              <td itemprop="dateVehicleFirstRegistered"><?php print render($content['field_make_year']); ?></td>
              <td class="noborder"></td>
              <td class="ad-data last-border"><?php print t('Registered City'); ?></td>
              <td class="last-border"><?php print render($content['field_registered_city']); ?></td>
            </tr>
            <tr>
              <td class="ad-data"><?php print t('Mileage'); ?></td>
              <td itemprop="dateVehicleFirstRegistered"><?php print render($content['field_mileage']); ?><?php print t('KM'); ?></td>
              <td class="noborder"></td>
              <td class="ad-data last-border"><?php print t('Color'); ?></td>
              <td class="last-border"><?php print render($content['field_color']); ?></td>
            </tr>
            <tr>
              <td class="ad-data"><?php print t('Transmission'); ?></td>
              <td ><?php print render($content['field_field_transmission']); ?></td>
              <td class="noborder"></td>
              <td class="ad-data last-border"><?php print t('Assembly'); ?></td>
              <td class="last-border"><?php print render($content['field_field_assembly']); ?></td>
            </tr>
            <tr>
              <td class="ad-data"><?php print t('Engine Capacity'); ?></td>
              <td><?php print render($content['field_engine_capacity']); ?><?php print t('cc'); ?></td>
              <td class="noborder"></td>
              <td class="ad-data last-border"><?php print t('Body Type'); ?></td>
              <td class="last-border"><?php print render($content['field_body_type']); ?></td>
            </tr>
            <tr>
              <td class="ad-data"><?php print t('Engine Type'); ?></td>
              <td itemprop="dateVehicleFirstRegistered"><?php print render($content['field_field_engine_type']); ?></td>
              <td class="noborder"></td>
              <td class="ad-data last-border"><?php print t('Last Updated:'); ?></td>
              <td class="last-border"><?php print format_date($content['classified_category']['#object']->changed); ?></td>
            </tr>
            <tr>
              <td class="ad-data"><?php print t('Ad Ref #'); ?></td>
              <td itemprop="dateVehicleFirstRegistered"><?php print render($content['classified_category']['#object']->vid); ?></td>
              <td class="noborder"></td>
              <td class="ad-data last-border"></td>
              <td class="last-border"></td>
            </tr>
            </tbody>
          </table>
          <h2 class="ad-detail-heading mt30">Car features</h2>
          <ul class="car-feature-list nomargin">
          	<?php 
				$features = render($content['field_features']); 
				$features_ = explode("  ", $features);
				foreach($features_ as $val): 
			?>
				<li><i class="fa fa-check-square"></i> <?php print $val; ?></li>	
			<?php endforeach; ?>
            
          </ul>
          <div class="clearfix"></div>
          <h2 class="ad-detail-heading mt30">Seller's Comments</h2>
          <div>
            <?php print render($content['body']); ?>
            <label class="detail-tip">When you call, please mention that you found this ad on PakWheels</label>
          </div>
          
          
    </div>
    <div id="already_requested_carsure" class="mb40">
          <div class="well text-center p40">
            <div class="carsure-logo-large mb5"></div>
            <p class="mb5" style="text-align:left;">Like this car, but not sure about its condition? Get this car inspected by CarSure and be sure with CarSure.</p>
            <p class="fs12 mb15"><a href="/products/carsure" target="_blank"><u>What is CarSure?</u></a></p>
            <input value="Request CarSure Report" class="btn-default call" data-toggle="modal" data-target="#car_sure_request_confirmation" onclick="showModal();" type="button">
          </div>
          </div>
  </div>
  <div class="col-md-4">
  	<div id="scrollToFixed">
    	<div class="side-bar">
        	<div class="well price-well">
            	<div class="price-box text-center"><?php print t('PKR'); ?> <?php print render($content['field_price_range']); ?></div>
                <button class="btn-default call text-center" style="font-size: 18px; width:100%;"><i class="fa fa-mobile"></i> <?php print render($content['field_phone']); ?></button>
                <a class="btn-block fs16 mt20 text-center"><i class="fa fa-envelope fa-2x"></i> Send a message</a>
                <div class="owner-detail-main">
                	<div class="owner-details">
                    	<div class="row-fluid">
                        	<div class="col-md-3 ftb"><?php print t('Dealer:'); ?></div>
                            <div class="span-9 ml7">Changezi Motors </div>
                        </div>
                        <div class="row-fluid">
                        	<div class="col-md-3 ftb"><?php print t('Address:'); ?></div>
                            <div class="span-9 ml7">Main double road, opposite PTCL exchange, Major Road, F-11 Markaz</div>
                        </div>
                        <div class="row-fluid">
                        	<div class="col-md-3 ftb"><?php print t('Timing:'); ?></div>
                            <div class="span-9 ml7">10:00 AM to 08:00 PM</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	
  	<div class="well mb10 mt20">
    	<h3 class="text-center">Safety tips for transaction</h3>
        <ol>
        	<li>Use a safe location to meet seller</li>
            <li>Avoid cash transactions</li>
            <li>Beware of unrealistic offers</li>
        </ol>
    </div>
  	<a href="#" class="report-ad text-center" style="display:block;"><i class="fa fa-flag fa-2x"></i> Report this ad</a>
  </div>
</div>