<ul class="autoshow-listing-teaser">
	<li class="listing-teaser">
    	<?php 
		/*echo '<pre>';
		print_r($row); 
		echo '</pre>';*/
		?>
    	<div class="span3 pull-left">
          <div class="img-box">
    		<?php print $fields['field_main_image']->content; ?>
          </div>
         </div>
         <div class="span9 pull-left">
         	<div class="row-fluid">
            	<h3><?php print $fields['title']->content; ?></h3>
            </div>
            <div class="row-fluid ">
            	<div class="span9 pull-left" style="margin-left:0;">
                	<ul class="search-vehicle-info">
                    	<li><?php print $fields['field_registered_city']->content; ?></li>
                        <li><?php print $fields['field_make_year']->content; ?></li>
                        <li><?php print $fields['field_mileage']->content; ?> <?php print t(' KM'); ?></li>
                    </ul>
                    <ul class="search-vehicle-info-1">
                    	<li><?php print $fields['field_field_engine_type']->content; ?></li>
                        <li><?php print $fields['field_engine_capacity']->content; ?><?php print t(' cc'); ?></li>
                        <li><?php print $fields['field_field_transmission']->content; ?></li>
                    </ul>
                </div>
                <div class="span3 pull-left">
                	<div class="price-detail">
                    	<span class="pkr"><?php print t('PKR '); ?></span><?php print $fields['field_price_range']->content; ?>
                    </div>
                </div>
            </div>
            <div class="row-fluid ">
            	<div class="pull-left dated"><?php print t('Latest Update: '); ?><?php print $fields['changed']->content; ?></div>
                <div class="pull-right"><button class="btn-default call"><i class="fa fa-mobile"></i> <?php print $fields['field_phone']->content; ?></button></div>
            </div>
         </div>
	</li>
</ul>