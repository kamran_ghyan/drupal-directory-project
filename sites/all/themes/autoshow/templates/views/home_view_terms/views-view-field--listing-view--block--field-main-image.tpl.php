<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>

<figure>
<?php print $output; ?>
  <div class="rating">
    <ul class="list-inline">
      <li><a href="#"><i class="fa fa-star"></i></a></li>
      <li><a href="#"><i class="fa fa-star"></i></a></li>
      <li><a href="#"><i class="fa fa-star"></i></a></li>
      <li><a href="#"><i class="fa fa-star-half-o"></i></a></li>
      <li><a href="#"><i class="fa fa-star-o"></i></a></li>
    </ul>

    <p>Featured</p>

  </div> <!-- end .rating -->

  <figcaption>
    <div class="bookmark">
      <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
    </div>
    <div class="read-more">
      <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
    </div>

  </figcaption>
</figure>
