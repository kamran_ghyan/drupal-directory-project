<div id="main-wrapper">
  <header id="header">
    <div class="header-top-bar">
      <div class="container">
        <!-- HEADER-LOGIN -->
        <div class="header-login">

          <?php
          global $user;

          if ( $user->uid ) {
            // Logged in user
          ?>
          <a href="user/logout" class=""><i class="fa fa-power-off"></i> Logout</a>
          <?php 
          }
          else {
            // Not logged in
          ?>
          <a href="#" class=""><i class="fa fa-power-off"></i> Login</a>

          <div>
            <form action="#">
              <input type="text" class="form-control" placeholder="Username">
              <input type="password" class="form-control" placeholder="Password">
              <input type="submit" class="btn btn-default" value="Login">
              <a href="#" class="btn btn-link">Forgot Password?</a>
            </form>
          </div>
          <?php
          }
          ?>


          

        </div> <!-- END .HEADER-LOGIN -->

        <!-- HEADER REGISTER -->
        <div class="header-register">
          <a href="#" class=""><i class="fa fa-plus-square"></i> Register</a>

          <div>
            <form action="#">
              <input type="text" class="form-control" placeholder="Username">
              <input type="email" class="form-control" placeholder="Email">
              <input type="password" class="form-control" placeholder="Password">
              <input type="submit" class="btn btn-default" value="Register">
            </form>
          </div>

        </div> <!-- END .HEADER-REGISTER -->

        <!-- HEADER-LOG0 -->
        <div class="header-logo text-center">
          <h2><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h2>
        </div>
        <!-- END HEADER LOGO -->

        <!-- CALL TO ACTION -->
        <div class="header-call-to-action">
          <a href="#" class="btn btn-default"><i class="fa fa-plus"></i> Add Listing</a>
        </div><!-- END .HEADER-CALL-TO-ACTION -->

      </div><!-- END .CONTAINER -->
    </div>
    <!-- END .HEADER-TOP-BAR -->

    <!-- HEADER SEARCH SECTION -->
    <div class="header-search slider-home">
      <div class="header-search-bar">
       <?php print render($page['top_search']); ?>
        
      </div> <!-- END .header-search-bar -->

      <div class="slider-content">

        <img alt="" src="<?php print $GLOBALS['base_url'].'/'.path_to_theme(); ?>/img/home-slide-img.jpg" style="height: 520px; width: 1803.92px;">
        <!-- end of masterslider -->
      </div> <!-- END .slider-content -->
    </div> <!-- END .SEARCH and slide-section -->

    <div class="container">
      <div class="header-nav-bar home-slide">
        <nav>

          <button><i class="fa fa-bars"></i></button>

          <?php
            $menu = menu_navigation_links('main-menu');
            print theme('links__system_main_menu', array('links' => $menu));
          ?>

          
        </nav>
      </div> <!-- end .header-nav-bar -->
    </div> <!-- end .container -->
  </header> <!-- end #header -->

  <div id="page-content" class="home-slider-content">
    <div class="container">
      <div class="home-with-slide">
        <div class="row">

		  <?php if($page['sidebar_first']): ?>
          <div class="col-md-9 col-md-push-3">
          <?php else : ?>
          <div class="col-md-12">
          <?php endif; ?>
          
            <div class="page-content">
            <?php if ($page['front_content_listing_main']): ?>
              <div class="change-view">
                <div class="filter-input">
                  <input type="text" placeholder="Filter by Categories" id="filter_cat">
                </div>
              </div> <!-- end .change-view -->


              <div class="product-details">
                <div class="tab-content">

                  <div class="tab-pane active" id="all-categories">
                    <h3><?php print $site_name; ?> <span>Categories</span></h3>

                    <?php print render($page['front_content_listing_main']); ?> 
                   
                  </div> <!-- end .tabe-pane -->


                  
                </div> <!-- end .tabe-content -->

              </div> <!-- end .product-details -->
             <?php endif ?>
             <?php if(!drupal_is_front_page()){ ?>
             <div class="product-details">
               <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
                  <a id="main-content"></a>
                  <?php /*?><?php print render($title_prefix); ?>
                  <?php if ($title): ?>
                    <h1 class="title" id="page-title">
                      <?php print $title; ?>
                    </h1>
                  <?php endif; ?>
                  <?php print render($title_suffix); ?>
                  <?php if ($tabs): ?>
                    <div class="tabs">
                      <?php print render($tabs); ?>
                    </div>
                  <?php endif; ?><?php */?>
                  <?php print render($page['help']); ?>
                  <?php if ($action_links): ?>
                    <ul class="action-links">
                      <?php print render($action_links); ?>
                    </ul>
                  <?php endif; ?>
                  <?php if($messages): ?>
                  <div id="messages">
                    <div class="section clearfix">
                      <?php print $messages; ?>
                    </div>
                  </div>
                  <?php endif; ?>
                  <?php print render($page['content']); ?>
                  
             </div>
             <?php } ?>
            </div> <!-- end .page-content -->
          </div>

          <div class="col-md-3 col-md-pull-9 category-toggle">

            <div class="page-sidebar">

              <!-- Category accordion -->
              <div id="categories">   
                <div class="accordion">
                <?php print render($page['sidebar_first']); ?>             
                </div>
              </div> <!-- end #categories -->

            </div> <!-- end .page-sidebar -->
          </div> <!-- end grid layout-->
        </div> <!-- end .row -->
      </div> <!-- end .home-with-slide -->
    </div> <!-- end .container -->
  </div>  <!-- end #page-content -->

<?php if ($page['featured_list']): ?>
  <div class="featured-listing" id= "featured-list">
    <div class="container">
      <div class="row clearfix">

          <h2><strong>Freatured</strong> Listing</h2>
          <?php print render($page['featured_list']); ?>

        </div>

      </div>  <!-- end .row -->

      
    </div>  <!-- end .container -->
  </div>  <!-- end .featured-listing -->
<?php endif; ?>
<?php if ($page['classified']): ?>
  <div class="classifieds-content">
    <div class="container">
    
      <!-- CLSSIFIEDS-CATEGROY BEGIN -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <h2><strong>Classfied</strong> Listing</h2>
          <?php print render($page['classified']); ?>

        </div> <!-- End grid layout -->

        

        </div> <!-- End grid layout -->
      </div> <!-- End .row -->
    </div> <!-- END .container-->
  </div> <!-- classifieds-content -->
<?php endif; ?>
  

  <footer id="footer">
    <div class="main-footer">

      <div class="container">
        <div class="row">

          <div class="col-md-3 col-sm-6">
            <div class="about-globo">
              <h3>About Globo</h3>

              <div class="footer-logo">
                <a href="#"><img src="img/footer_logo.png" alt=""></a>
                <span></span> <!-- This content for overlay effect -->
              </div>

              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.</p>

            </div> <!-- End .about-globo -->
          </div> <!-- end Grid layout-->

          <div class="col-md-3 col-sm-6">
            <h3>Latest From Blog</h3>

            <div class="latest-post clearfix">
              <div class="post-image">
                <img src="img/content/latest_post_1.jpg" alt="">

                <p><span>12</span>Sep</p>
              </div>

              <h4><a href="#">Post Title Goes Here</a></h4>

              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            </div>

            <div class="latest-post clearfix">
              <div class="post-image">
                <img src="img/content/latest_post_2.jpg" alt="">

                <p><span>09</span>Sep</p>
              </div>

              <h4><a href="#">Post Title Goes Here</a></h4>

              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            </div>
          </div> <!-- end Grid layout-->

          <div class="col-md-3 col-sm-6 clearfix">
            <div class="popular-categories">
              <h3>Popular Categories</h3>

              <ul>
                <li><a href="#"><i class="fa fa-shopping-cart"></i>E-commerce</a></li>
                <li><a href="#"><i class="fa fa-paper-plane-o"></i>Entertainment</a></li>
                <li><a href="#"><i class="fa fa-cogs"></i>Industry</a></li>
                <li><a href="#"><i class="fa fa-book"></i>Libraries &amp; Public Office</a></li>
                <li><a href="#"><i class="fa fa-building-o"></i>Real Estate</a></li>
              </ul>
            </div> <!-- end .popular-categories-->
          </div> <!-- end Grid layout-->

          <div class="col-md-3 col-sm-6">
            <div class="newsletter">
              <h3>Newsletter</h3>

              <form action="#">
                <input type="Email" placeholder="Email address">
                <button><i class="fa fa-plus"></i></button>
              </form>

              <h3>Keep In Touch</h3>

              <ul class="list-inline">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div> <!-- end .newsletter-->

          </div> <!-- end Grid layout-->
        </div> <!-- end .row -->
      </div> <!-- end .container -->
    </div> <!-- end .main-footer -->

    <div class="copyright">
      <div class="container">
        <?php if ($page['footer']): ?>   
            <?php print render($page['footer']); ?>          
        <?php endif; ?>
        <ul class="list-inline">
          <li><a href="#">Home</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Shortcodes</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Pricing</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
      </div> <!-- END .container -->
    </div> <!-- end .copyright-->
  </footer> <!-- end #footer -->



</div> <!-- end #main-wrapper -->
