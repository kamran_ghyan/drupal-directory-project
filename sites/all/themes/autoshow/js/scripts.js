(function($) {
Drupal.behaviors.myBehavior = {
  attach: function (context, settings) {


	// Show Font Awesome icons for category Listing
    $(".menu-block-2 ul.menu li a").prepend(" <i class='fa fa-car'></i>");

    // Home sidebar category listing
	$('#block-menu-block-1 ul.menu').each(function(){
		$(this).find('li.leaf').hide().filter(':lt(20)').show();
	}); 
	// check the size of li and show more button if size is greater then 5
	$('#block-menu-block-1 ul.menu').each(function(){
		
		var moretext = '<i class="fa fa-plus-square-o"></i> More';
		var lesstext = '<i class="fa fa-plus-square-o"></i> Less';

		// check size of li
		var size_ = $(this).find('li.leaf').size();
		// check to show button or not
		if(size_ > 15){
			// append a li with text more that on click will show the rest
			$(this).append('<li class="btn more btn-default text-center" >'+moretext+'</li>').find('li:last').click(function(){		  

			if($(this).hasClass("more")) {
            	$(this).removeClass("more");
            	$(this).html(lesstext);
	        } else {
	            $(this).addClass("more");
	            $(this).html(moretext);
	        }
	        //$(this).parent().prev().toggle();
	        //$(this).prev().toggle();

	        $(this).siblings(':gt(5)').slideToggle(500); 
		});
		} else{
		}

	});	

	$('#block-menu-block-2 ul.menu').each(function(){
			$(this).find('li.leaf').hide().filter(':lt(20)').show();
		}); 
		// check the size of li and show more button if size is greater then 5
		$('#block-menu-block-2 ul.menu').each(function(){
			
			var moretext = '<i class="fa fa-plus-square-o"></i> Discover More';
			var lesstext = '<i class="fa fa-plus-square-o"></i> Discover Less';

			// check size of li
			var size_ = $(this).find('li.leaf').size();
			// check to show button or not
			if(size_ > 15){
				// append a li with text more that on click will show the rest
				$(this).append('<li class="btn more btn-default text-center discover" >'+moretext+'</li>').find('li:last').click(function(){		  

				if($(this).hasClass("more")) {
	            	$(this).removeClass("more");
	            	$(this).html(lesstext);
		        } else {
		            $(this).addClass("more");
		            $(this).html(moretext);
		        }
		        //$(this).parent().prev().toggle();
		        //$(this).prev().toggle();

		        $(this).siblings(':gt(5)').slideToggle(500); 
			});
			} else{
			}

		});	


    
    // Show li for classfied module block
	$('#block-menu-menu-taxonomy ul li.expanded ul').each(function(){
		$(this).find('li.leaf').hide().filter(':lt(5)').show();
	}); 

	
	// check the size of li and show more button if size is greater then 5
	$('#block-menu-menu-taxonomy ul li.expanded ul').each(function(){
		
		var moretext = '<i class="fa fa-plus-square-o"></i> More';
		var lesstext = '<i class="fa fa-plus-square-o"></i> Less';

		// check size of li
		var size_ = $(this).find('li.leaf').size();
		// check to show button or not
		if(size_ > 6){
			// append a li with text more that on click will show the rest
			$(this).append('<li class="btn more btn-default text-center" >'+moretext+'</li>').find('li:last').click(function(){		  

			if($(this).hasClass("more")) {
            	$(this).removeClass("more");
            	$(this).html(lesstext);
	        } else {
	            $(this).addClass("more");
	            $(this).html(moretext);
	        }
	        //$(this).parent().prev().toggle();
	        //$(this).prev().toggle();

	        $(this).siblings(':gt(5)').slideToggle(500); 
		});
		} else{
		}

	});	

	

	// Search by keywords using clossed
	$( "#filter_cat" ).keypress(function() {
		// Search query
	    var query = $('#filter_cat').val().toLowerCase();
	    // show appropriate result and hide rest of the li's
	    $('.menu-block-2 ul.menu li.leaf a').each(function(){
	        var $this = $(this);
	        if($this.text().toLowerCase().indexOf(query) === -1)
	          $this.closest('.menu-block-2 ul.menu li.leaf').fadeOut();
	        else $this.closest('.menu-block-2 ul.menu li.leaf').fadeIn();
	    });
	});

	// Listing page search filter

	// Show li for classfied module block
	$('#block-menu-menu-taxonomy ul li.expanded ul').each(function(){
		$(this).find('li.leaf').hide().filter(':lt(5)').show();
	});

	// Show li for classfied module block
	$('#custom-search-blocks-form-2 .fieldset-wrapper div').each(function(){
		$(this).find('div.form-item').hide().filter('div.form-item:lt(5)').show();
	}); 

	// check the size of li and show more button if size is greater then 5
	$('#custom-search-blocks-form-2 .fieldset-wrapper div').each(function(){
		
		var moretext = '<i class="fa fa-plus-square-o"></i> More...';
		var lesstext = '<i class="fa fa-plus-square-o"></i> Less';

		// check size of li
		var size_ = $(this).find('div.form-item').size();
		// check to show button or not
		if(size_ > 5){
			// append a li with text more that on click will show the rest
			$(this).append('<li style="padding:0 10px;" class="btn more btn-default text-center" >'+moretext+'</li>').find('div:last').click(function(){		  

			if($(this).hasClass("more")) {
            	$(this).removeClass("more");
            	$(this).html(lesstext);
	        } else {
	            $(this).addClass("more");
	            $(this).html(moretext);
	        }
	        //$(this).parent().prev().toggle();
	        //$(this).prev().toggle();

	        $(this).siblings(':nth-of-type(5)').slideToggle(500); 
		});
		} else{}

	});	
	
	


  }
};
})(jQuery);
