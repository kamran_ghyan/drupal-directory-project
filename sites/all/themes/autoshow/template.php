<?php
function autoshow_links__system_main_menu($variables) {

  $html .= "<ul class='primary-nav list-unstyled'>";  
  foreach ($variables['links'] as $key => $link) {
    $html .= "<li>".l($link['title'], $link['path'], $link)."</li>";
  }
  $html .= "  </ul>";
  
  return $html;
}

function autoshow_form_custom_search_blocks_form_1_alter(&$form, &$form_state, $form_id) {

    $form['search_block_form']['#title'] = t('Search'); // Change the text on the label element
    $form['search_block_form']['#size'] = 40;  // define size of the textfield
    $form['search_block_form']['#attributes'] = array('class' => 'keyword'); 
    $form['search_block_form']['#default_value'] = t('Search'); // Set a default value for the textfield

    $form['cities'] = array(
       '#type' => 'select',
       '#attributes' => array('class' => 'location'),
       '#options' => get_list_select('cities'),
       '#default_value' => $category['selected'],
   );

    $form['categories'] = array(
       '#type' => 'select',
       '#attributes' => array('class' => 'categories'),
       '#options' => get_list_select('classified_categories'),
       '#default_value' => $category['selected'],
   );
  $form['actions']['submit']['#attributes'] = array('class' => array('side-bar-search'));
  $form['actions']['submit']['#value'] = t('GO'); // Change the text on the submit button


  // Alternative (HTML5) placeholder attribute instead of using the javascript
  $form['search_block_form']['#attributes']['placeholder'] = t('Search');

}
/**
* hook_form_FORM_ID_alter
*/
function autoshow_form_search_block_form_alter(&$form, &$form_state, $form_id) {

    $form['search_block_form']['#title'] = t('Search'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = true; // Toggle label visibilty
    $form['search_block_form']['#size'] = 40;  // define size of the textfield

    $form['search_block_form']['#default_value'] = t('Search'); // Set a default value for the textfield

   /* $form['cities'] = array(
       '#type' => 'select',
       '#title' => t('Location'),
       '#options' => get_list_select('cities'),
       '#default_value' => $category['selected'],
   );

    $form['categories'] = array(
       '#type' => 'select',
       '#title' => t('Category'),
       '#options' => get_list_select('classified_categories'),
       '#default_value' => $category['selected'],
   );*/
  

  $form['actions']['submit']['#attributes'] = array('class' => array('side-bar-search'));
  $form['actions']['submit']['#value'] = t('GO'); // Change the text on the submit button


  // Alternative (HTML5) placeholder attribute instead of using the javascript
  $form['search_block_form']['#attributes']['placeholder'] = t('Search');
} 

// Listing page search filter
function autoshow_form_custom_search_blocks_form_2_alter(&$form, &$form_state, $form_id) {

  // Keywords input type
  $form['search_block_form'] = array(
       '#title' => t('Search'),
       '#attsizeributes' => 40,
       '#attributes' => array('class' => 'keyword'),
       '#default_value' => t('Search'),
   );
  // Carsure field set
  $form['carsure_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Carsure'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );
  // Carsue radio input type
  $form['carsure_fieldset']['carsure'] = array(
     '#type' => 'checkboxes',
     '#attributes' => array('class' => 'carsure'),
     '#options' => get_list('carsure'),
  );
  // Cities fieldset
  $form['cities_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('City'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );
  // Cities select list
  $form['cities_fieldset']['cities'] = array(
     '#type' => 'checkboxes',
     '#attributes' => array('class' => 'location'),
     '#options' => get_list('cities'),
     '#default_value' => $category['selected'],
  );

  // Categories fieldset
  $form['categories_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Make'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );
  // Categories select list
  $form['categories_fieldset']['categories'] = array(
       '#type' => 'checkboxes',
       '#attributes' => array('class' => 'categories'),
       '#options' => get_list('classified_categories'),
       '#default_value' => $category['selected'],
   );

  // price range fieldset
  $form['price_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Price Range'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Price select list
  $form['price_fieldset']['price'] = array(
       '#type' => 'select',
       '#options' => get_list_select('price_range'),
       '#default_value' => $category['selected'],
   );

  // register fieldset
  $form['register_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Register City'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );
  // register select list
  $form['register_fieldset']['register'] = array(
       '#type' => 'checkboxes',
       '#options' => get_list('cities'),
       '#default_value' => $category['selected'],
   );

  // Years fieldset
  $form['years_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Years'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Years select list
  $form['years_fieldset']['years'] = array(
       '#type' => 'checkboxes',
       '#options' => get_list('make_year'),
       '#default_value' => $category['selected'],
   );

  // Engine type fieldset
  $form['engine_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Engine Type'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Engine type select list
  $form['engine_fieldset']['engine'] = array(
       '#type' => 'checkboxes',
       '#options' => get_list('engine_type'),
       '#default_value' => $category['selected'],
   );

  // Engine capacity fieldset
  $form['capacity_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Engine capacity (CC)'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Engine capacity select list
  $form['capacity_fieldset']['capacity'] = array(
       '#type' => 'select',
       '#options' => get_list_select('engine_capacity'),
       '#default_value' => $category['selected'],
   );

  // Mileage fieldset
  $form['mileage_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Mileage (KM)'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Mileage select list
  $form['mileage_fieldset']['mileage'] = array(
       '#type' => 'select',
       '#options' => get_list_select('mileage'),
       '#default_value' => $category['selected'],
   );

  // Color fieldset
  $form['color_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Color'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Color select list
  $form['color_fieldset']['color'] = array(
       '#type' => 'checkboxes',
       '#options' => get_list('color'),
       '#default_value' => $category['selected'],
   );

  // Picture fieldset
  $form['picture_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Picture Availability'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Picture select list
  $form['picture_fieldset']['picture'] = array(
       '#type' => 'checkboxes',
       '#options' => get_list('picture_avialabi'),
       '#default_value' => $category['selected'],
   );

  // Body type fieldset
  $form['body_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Body type'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Body type select list
  $form['body_fieldset']['body'] = array(
       '#type' => 'checkboxes',
       '#options' => get_list_select('body_type'),
       '#default_value' => $category['selected'],
   );

  // Assembly type fieldset
  $form['assembly_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Assembly'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Assembly type select list
  $form['assembly_fieldset']['assembly'] = array(
       '#type' => 'checkboxes',
       '#options' => get_list('assembly'),
       '#default_value' => $category['selected'],
   );

  // Transmission type fieldset
  $form['transmission_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Transmission'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Transmission type select list
  $form['transmission_fieldset']['transmission'] = array(
       '#type' => 'checkboxes',
       '#options' => get_list_select('transmission'),
       '#default_value' => $category['selected'],
   );

  // Seller type fieldset
  $form['seller_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Seller type'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Seller type select list
  $form['seller_fieldset']['seller'] = array(
       '#type' => 'checkboxes',
       '#options' => get_list_select('seller_type'),
       '#default_value' => $category['selected'],
   );

  // AD type fieldset
  $form['adtype_fieldset'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Ad type'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // AD type select list
  $form['adtype_fieldset']['ad'] = array(
       '#type' => 'checkboxes',
       '#options' => get_list_select('ad_type'),
       '#default_value' => $category['selected'],
   );
  
  $form['actions']['submit']['#attributes'] = array('class' => array('side-bar-search'));
  $form['actions']['submit']['#value'] = t('GO!'); // Change the text on the submit button


  // Alternative (HTML5) placeholder attribute instead of using the javascript
  $form['search_block_form']['#attributes']['placeholder'] = t('Search');

}

// list value for custom enhanced search form
function get_list_select($param){
  // Emoty value for select list
  $option_value = array( 0 => 'Select One');
  // Get an single value of specific term
  $term_name = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name=:name ", array(':name' => $param))->fetchField();
  // Get an associative array of specific terms
  $term_list = db_query("SELECT name FROM {taxonomy_term_data} WHERE vid=:vid ", array(':vid' => $term_name))->fetchAllKeyed(0,0);
  return array_merge($option_value,$term_list);
}

// list value for custom enhanced search form
function get_list($param){

  // Get an single value of specific term
  $term_name = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name=:name ", array(':name' => $param))->fetchField();
  // Get an associative array of specific terms
  $term_list = db_query("SELECT name FROM {taxonomy_term_data} WHERE vid=:vid ", array(':vid' => $term_name))->fetchAllKeyed(0,0);
  return $term_list;
}



function autoshow_facetapi_default_title(){
  return t('Filter by @title:', array('@title' => drupal_strtolower($variables['title'])));
}


function autoshow_preprocess_page(&$variables, $hook) {  // Add theme suggestion for all content types
    if (isset($variables['node'])) {
        if ($variables['node']->type != '') {
          $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
        }
    }
}




